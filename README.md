# Gdevelop Games I made during TriJam 2021

This a compilation of four of my games, I made during several TriJam in 2021. <br>
In this repository you'll find complete access to my source code.

<b>[Match'it](https://lumbirbwut.itch.io/matchit) <br> </b>
Trijam#102 : Contrast
<figure>
  <img src="Pictures/MatchIt.png"
       width="250">
</figure>

---

<b> [A Family Tree](https://lumbirbwut.itch.io/a-family-tree) <br> </b>
Trijam#103 : A Family Tree

<figure>
  <img src="Pictures/FamilyTree.png"
       width="250">
</figure>

---

<b> [Who let the dog out ?](https://lumbirbwut.itch.io/who-let-the-dogs-out) <br> </b>
TriJam#104:  : Who let the dog out ?

<figure>
  <img src="Pictures/WhoLetTheDogsOut.png"
       width="250">
</figure>

---

<b> [Ring Bonney's Treasure](https://lumbirbwut.itch.io/ring-bonneys-treasure) <br> </b>
TriJam#105: Islands

<figure>
  <img src="Pictures/RingBonneysTreasure.png"
       width="250">
</figure>
